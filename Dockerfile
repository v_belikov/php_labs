FROM debian:jessie
RUN apt-get update && apt-get -y install php5 php5-mysql php5-curl php5-gd php5-xdebug && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN echo "xdebug.remote_enable=1" >> /etc/php5/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_autostart=1" >> /etc/php5/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_handler=dbgp" >> /etc/php5/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_connect_back=1" >> /etc/php5/apache2/conf.d/20-xdebug.ini
RUN echo "xdebug.remote_port=9000" >> /etc/php5/apache2/conf.d/20-xdebug.ini
RUN a2enmod rewrite
COPY conf.d/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN /usr/sbin/a2dismod 'mpm_*' && /usr/sbin/a2enmod mpm_prefork
EXPOSE 80 
EXPOSE 443 
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
